# robots

Install.:

- git clone
- create database (utf8_general_ci)
- setup .env file
    - DB_DATABASE
    - DB_USERNAME
    - DB_PASSWORD
- composer Install
- npm Install
- npm run prod
- php artisan migrate
- php artisan db:seed
- php artisan serve

