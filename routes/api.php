<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RobotController;

Route::get('types', [RobotController::class, 'types']);
Route::get('/', [RobotController::class, 'list']);

Route::post('/', [RobotController::class, 'store']);
Route::post('/fight', [RobotController::class, 'fight']);
Route::post('/delete', [RobotController::class, 'delete']);
Route::post('/update', [RobotController::class, 'update']);
