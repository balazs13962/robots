<?php

namespace App\Repositories;

use App\Models\Robot;
use PhpParser\ErrorHandler\Collecting;
use Illuminate\Support\Collection;
use Carbon\Carbon;

class RobotRepository
{

    /**
     * all
     *
     * @return RobotRepository
     */
    public static function init(): RobotRepository
    {
        return new RobotRepository;
    }

    /**
     * list
     *
     * @return Collection
     */
    public function all(): Collection
    {
        $robots = collect();

        foreach (Robot::all() as $robot) {
            $robots->push([
                'id' => $robot->id,
                'name' => $robot->name,
                'power' => $robot->power,
                'type' => $robot->type->name,
            ]);
        }
        return $robots;
    }

    /**
     * fight
     * 
     * Decision by power, if equals thw winner depends on creation date
     *
     * @return void
     */
    public function fight()
    {
        $firstRobot = Robot::find(request()->ids[0]);
        $secondRobot = Robot::find(request()->ids[1]);

        if ($firstRobot->power > $secondRobot->power) {
            return $firstRobot->attributesToArray();
        } else if ($secondRobot->power > $firstRobot->power) {
            return  $secondRobot->attributesToArray();
        }

        return (new Carbon($firstRobot->created_at))
            >
            (new Carbon($secondRobot->created_at))
            ?
            $firstRobot->attributesToArray()
            :
            $secondRobot->attributesToArray();
    }
}
