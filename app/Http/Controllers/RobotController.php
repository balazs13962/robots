<?php

namespace App\Http\Controllers;

use App\Models\RobotType;
use App\Models\Robot;
use App\Http\Requests\StorePostRequest;
use App\Http\Requests\FightPostRequest;
use App\Http\Requests\DeletePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Repositories\RobotRepository;

class RobotController extends Controller
{

    /**
     * index
     * 
     * Return view of home page
     *
     * @return void
     */
    public function index()
    {
        return view('index');
    }

    /**
     * create
     * 
     * Return view of creation
     *
     * @return void
     */
    public function create()
    {
        return view('create', ['robotDetail' => null]);
    }

    /**
     * store
     * 
     * Request validation is written in StorePostRequest class
     *
     * @param  mixed $req
     * @return void
     */
    public function store(StorePostRequest $req)
    {
        return response()->json([
            Robot::updateOrCreate(request()->all())
        ], 200);
    }

    /**
     * fight
     * 
     * Handle two robot fight
     * 
     * Request validation is written in FightPostRequest class
     *
     * @param  mixed $req
     * @return void
     */
    public function fight(FightPostRequest $req)
    {
        return RobotRepository::init()->fight();
    }

    /**
     * list
     * 
     * Return all robot details
     *
     * @return void
     */
    public function list()
    {
        return RobotRepository::init()->all();
    }

    /**
     * types
     * 
     * Return with Robot types
     *
     * @return void
     */
    public function types()
    {
        return RobotType::all();
    }

    /**
     * delete
     * 
     * Soft deletion
     * 
     * Request validation is written in DeletePostRequest class
     *
     * @return void
     */
    public function delete(DeletePostRequest $req)
    {
        return Robot::findOrFail($req->id)->delete();
    }

    /**
     * edit
     * 
     * Request validation is written in DeletePostRequest class
     *
     * @return void
     */
    public function edit()
    {
        return view('create', ['robotDetail' => Robot::findOrFail(request()->id)->attributesToArray()]);
    }

    public function update(UpdatePostRequest $req)
    {
        return
            Robot::where('id', $req->id)
            ->update($req->except('id'));
    }
}
