<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Robot extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['name', 'power', 'type_id'];

    public function type()
    {
        return $this->belongsTo(RobotType::class);
    }

    public function toList()
    {
    }
}
