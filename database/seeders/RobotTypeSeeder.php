<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\RobotType;

class RobotTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedasdasd([
            'brawler',
            'rouge',
            'assault',
        ]);
    }

    /**
     * seed
     *
     * @param  mixed $names
     * @return void
     */
    private function seedasdasd(array $names)
    {
        foreach ($names as $name) {
            RobotType::updateOrCreate([
                'name' => $name
            ]);
        }
    }
}
