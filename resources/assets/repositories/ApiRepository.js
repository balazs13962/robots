import Repository from './Repository'
let resource = 'api'

export default {

    // Get Robot details
    list(params) {
        return Repository.get(resource, params)
    },

    getRobotTypes() {
        return Repository.get(resource + "/types")
    },

    store(params) {
        return Repository.post(resource, params)
    },

    fight(params) {
        return Repository.post(resource + "/fight", params)
    },

    delete(params) {
        return Repository.post(resource + "/delete", params)
    },

    update(params) {
        return Repository.post(resource + "/update", params)
    }
}
