import axios from 'axios'

const baseURL = Application.url

export default axios.create({
    baseURL
})
