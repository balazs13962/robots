import Vue from 'vue'
import Vuex from 'vuex'
import IndexContainer from './IndexContainer.vue'

window.axios = require('axios')
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
const token = document.head.querySelector('meta[name="csrf-token"]')

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content
}

const app = new Vue({
    el: '#app',
    components: { IndexContainer }
    // i18n,
    // store
});
