@php
$config = [
'appName' => config('app.name'),
'locale' => $locale = app()->getLocale(),
'locales' => config('app.locales')
];
@endphp
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- SEO Meta Tags -->
    <meta name="description" content="This is the page description">
    <meta name="author" content="balazs13962">

    <!-- Website Title -->
    <title>{{ config('app.name') }}</title>

    <!-- Styles -->

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700&display=swap&subset=latin-ext" rel="stylesheet">
    <link href="custom/css/bootstrap.css" rel="stylesheet">
    <link href="custom/css/fontawesome-all.css" rel="stylesheet">
    <link href="custom/css/swiper.css" rel="stylesheet">
    <link href="custom/css/magnific-popup.css" rel="stylesheet">
    <link href="custom/css/styles.css" rel="stylesheet">

</head>

<body data-spy="scroll" data-target=".fixed-top">

    <!-- Preloader -->
    <div class="spinner-wrapper">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>
    <!-- end of preloader -->

    <div class="container">
        @yield('content')
    </div>

    <!-- Copyright -->
    <div class="copyright mt-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <p class="p-small">Copyright © 2021 <a href="https://gitlab.com/balazs13962">Szedlacsek Balázs</a></p>
                </div>
            </div> <!-- enf of row -->
        </div>
    </div> <!-- end of copyright -->
    <!-- end of copyright -->


    <!-- Scripts -->
    <script src="custom/js/jquery.min.js"></script> <!-- jQuery for Bootstrap's JavaScript plugins -->
    <script src="custom/js/popper.min.js"></script> <!-- Popper tooltip library for Bootstrap -->
    <script src="custom/js/bootstrap.min.js"></script> <!-- Bootstrap framework -->
    <script src="custom/js/jquery.easing.min.js"></script> <!-- jQuery Easing for smooth scrolling between anchors -->
    <script src="custom/js/swiper.min.js"></script> <!-- Swiper for image and text sliders -->
    <script src="custom/js/jquery.magnific-popup.js"></script> <!-- Magnific Popup for lightboxes -->
    <script src="custom/js/validator.min.js"></script> <!-- Validator.js - Bootstrap plugin that validates forms -->
    <script src="custom/js/scripts.js"></script> <!-- Custom scripts -->

    <script type="text/javascript">
        var Application = {
            lang: '{{ app()->getLocale() }}',
            fallbackLocale: '{{ config('
            app.fallback_locale ') }}',
            url: '{{ url("") }}'
        }
    </script>

    @stack('scripts')
</body>

</html>