@extends('layouts.wrapper')

@push('scripts')
<script src="{{ url('/js/create/index.js') }}"></script>
@endpush

@section('content')
<div id="app" class="d-flex justify-content-center mt-2">
    <create-container :robot-detail="{{ json_encode($robotDetail) }}" />
</div>

@endsection