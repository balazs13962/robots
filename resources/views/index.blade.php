@extends('layouts.wrapper')

@push('scripts')
<script src="{{ url('/js/index.js') }}"></script>
@endpush

@section('content')
<div id="app" class="d-flex justify-content-center mt-2">
    <index-container />
</div>
@endsection